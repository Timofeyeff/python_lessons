from math import sqrt


def my_s(a, b, c):
    p = (a + b + c) / 2
    s = sqrt(p * (p - a) * (p - b) * (p - c))
    return (s)


if __name__ == "__main__":
    val_a = input("Введите сторону А: ")
    val_b = input("Введите сторону В: ")
    val_c = input("Введите сторону С: ")

    if len(val_a) > 0 and len(val_b) > 0 and len(val_c) > 0:
        a = float(val_a)
        b = float(val_b)
        c = float(val_c)
        s = my_s(a, b, c)
        print("Площадь треугольника равна:", s)
    else:
        print("Ошибка ввода! Значения не могут быть пустыми!")
