def my_max(x, y):
    if x > y:
        return (x)
    else:
        return (y)


in1 = input("Введите первое число: ")
in2 = input("Введите второе число: ")

if len(in1) > 0 or len(in2) > 0:
    x = float(in1)
    y = float(in2)
    print("Наибольшее число:", my_max(x, y))
else:
    print('Ошибка! Вы ввели пустое значение!')
