from random import randint


def my_rand_game(x):
    y = randint(1, 100)
    if x == y:
        print("Поздравляю! Вы угадали")
    else:
        print("К сожалению, вы ошиблись. Правильное число", y)

if __name__ == "__main__":
    print("Загадываю число от 1 до 100. Попробуйте его угадать")
    val = input("Введите свой вариант числа от 1 до 100: ")
    if len(val) > 0:
        my_rand_game(val)
    else:
        print("Ошибка! Необходимо ввести число!")
