from math import pow


def func_pow(x):
    """Вычисляет уравнение в 4 степени

    >>> func_pow(3)
    93.0

    >>> func_pow(5)
    645.0
    """
    return (pow(x, 4) + 4 * x)

import doctest
doctest.testmod()

