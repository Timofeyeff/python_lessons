def my_calc(x):
    if -2.4 <= x <= 5.7:
        return (pow(x, 2))
    else:
        return (4)


val = input("Введите число: ")
if len(val) > 0:
    x = float(val)
    print("Результат равен", my_calc(x))
else:
    print("Ошибка! Необходимо ввести вещественное число")
