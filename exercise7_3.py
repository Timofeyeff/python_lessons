def my_parity(x):
    if x % 2 > 0:
        return (False)
    else:
        return (True)


in1 = input("Введите число: ")
if len(in1) > 0:
    x = int(in1)
    if my_parity(x):
        print("Число", x, "четное")
    else:
        print("Число", x, "нечетное")
else:
    print("Ошибка! Вы ввели пустое значение")
