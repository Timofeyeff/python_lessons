def get_material(pH):
    value = ("Не знаю, что это :-(")
    if pH == 3.0:
        value = ("Яблочный сок")
    elif pH == 5.5:
        value = ("Шампунь")
    elif 9.0 <= pH <= 10.0:
        value = ("Мыло для рук")
    return (value)


value = input("Введите pH: ")

if len(value) > 0:
    pH = float(value)
    if 1.0 <= pH <= 14.0:
        print(get_material(pH))
    else:
        print("Неправильный ввод, pH должен быть числом от 1 до 14")
else:
    print("Неправильный ввод, строка не должна быть пустой. Необходимо ввести число от 1 до 14")
