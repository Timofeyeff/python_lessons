from math import sqrt
from math import sin


def my_sin(x):
    return (sqrt(1 - sin(x) ** 2))


if __name__ == "__main__":
    val = input("Введите х: ")
    if len(val) > 0:
        x = float(val)
        print("Вычисленный результат =", my_sin(x))
    else:
        print("Ошибка! Необходимо ввести число!")
